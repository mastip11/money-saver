package mastip.com.moneysaver.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Tivo Yudha on 26/04/2017.
 */

public class TransactionModel implements Parcelable {

    private Integer ID;
    private String Type;
    private Integer Balance;
    private String Reason;
    private String TimeOccured;

    public TransactionModel() {

    }

    public TransactionModel(Integer ID, String Type, Integer Balance, String Reason, String TimeOccured) {
        this.ID = ID;
        this.Type = Type;
        this.Balance = Balance;
        this.Reason = Reason;
        this.TimeOccured = TimeOccured;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setType(String type) {
        Type = type;
    }

    public void setBalance(Integer balance) {
        Balance = balance;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public void setTimeOccured(String timeOccured) {
        TimeOccured = timeOccured;
    }

    public Integer getID() {
        return ID;
    }

    public Integer getBalance() {
        return Balance;
    }

    public String getReason() {
        return Reason;
    }

    public String getTimeOccured() {
        return TimeOccured;
    }

    public String getType() {
        return Type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.ID);
        dest.writeString(this.Type);
        dest.writeValue(this.Balance);
        dest.writeString(this.Reason);
        dest.writeString(this.TimeOccured);
    }

    protected TransactionModel(Parcel in) {
        this.ID = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Type = in.readString();
        this.Balance = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Reason = in.readString();
        this.TimeOccured = in.readString();
    }

    public static final Parcelable.Creator<TransactionModel> CREATOR = new Parcelable.Creator<TransactionModel>() {
        @Override
        public TransactionModel createFromParcel(Parcel source) {
            return new TransactionModel(source);
        }

        @Override
        public TransactionModel[] newArray(int size) {
            return new TransactionModel[size];
        }
    };
}

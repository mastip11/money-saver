package mastip.com.moneysaver.model;

import java.util.ArrayList;

/**
 * Created by Tivo Yudha on 04/05/2017.
 */

public class DataHolder {

    private static DataHolder singleton;

    public static DataHolder getSingleton() {
        if(singleton == null) {
            singleton = new DataHolder();
        }
        return singleton;
    }

    private static ArrayList<DataUserModel> dataUserModels;

    public static void setDataUserModels(ArrayList<DataUserModel> dataUserModels) {
        DataHolder.dataUserModels = dataUserModels;
    }

    public static ArrayList<DataUserModel> getDataUserModels() {
        return dataUserModels;
    }
}

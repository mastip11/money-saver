package mastip.com.moneysaver.model;

/**
 * Created by Tivo Yudha on 01/05/2017.
 */

public class DataUserModel {

    private static Integer userID;
    private static String fullname;
    private static String createdAt;
    private static Integer balance;

    public static Integer getUserID() {
        return userID;
    }

    public static void setUserID(Integer userID) {
        DataUserModel.userID = userID;
    }

    public static String getFullname() {
        return fullname;
    }

    public static void setFullname(String fullname) {
        DataUserModel.fullname = fullname;
    }

    public static String getCreatedAt() {
        return createdAt;
    }

    public static void setCreatedAt(String createdAt) {
        DataUserModel.createdAt = createdAt;
    }

    public static Integer getBalance() {
        return balance;
    }

    public static void setBalance(Integer balance) {
        DataUserModel.balance = balance;
    }
}

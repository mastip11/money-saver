package mastip.com.moneysaver.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.text.NumberFormat;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.model.TransactionModel;
import mastip.com.moneysaver.support.extend.view.MSTextView;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class ListHistoryViewHolder extends RecyclerView.ViewHolder {

    private MSTextView tvTransactionBalance;
    private MSTextView tvTransactionReason;
    private MSTextView tvTransactionTimeOccured;
    private MSTextView tvTransactionType;

    private View v;

    public ListHistoryViewHolder(View v) {
        super(v);

        this.v = v;

        tvTransactionBalance = (MSTextView) v.findViewById(R.id.tvTransactionBalance);
        tvTransactionReason = (MSTextView) v.findViewById(R.id.tvTransactionReason);
        tvTransactionTimeOccured = (MSTextView) v.findViewById(R.id.tvTransactionTimeOccured);
        tvTransactionType = (MSTextView) v.findViewById(R.id.tvTransactionType);
    }

    public void setUpModelToUI(TransactionModel data) {
        tvTransactionBalance.setText(NumberFormat.getInstance().format(data.getBalance()) + "");
        tvTransactionReason.setText(data.getReason());
        tvTransactionTimeOccured.setText(data.getTimeOccured());
        tvTransactionType.setText(v.getContext().getString(R.string.text_transaction_type) + data.getType());
    }
}

package mastip.com.moneysaver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.adapter.viewholder.ListHistoryViewHolder;
import mastip.com.moneysaver.model.TransactionModel;

/**
 * Created by Tivo Yudha on 26/04/2017.
 */

public class ListHistoryAdapter extends RecyclerView.Adapter<ListHistoryViewHolder> {

    private Context context;
    private ArrayList<TransactionModel> allData = new ArrayList<>();

    public ListHistoryAdapter(Context context, ArrayList allData) {
        this.context = context;
        this.allData = allData;
    }

    @Override
    public void onBindViewHolder(ListHistoryViewHolder holder, int position) {
        holder.setUpModelToUI(allData.get(position));
    }

    @Override
    public ListHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_layout_list_transaction, null);
        return new ListHistoryViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return allData.size();
    }
}

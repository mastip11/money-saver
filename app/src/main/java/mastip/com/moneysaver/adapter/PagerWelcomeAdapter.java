package mastip.com.moneysaver.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.view.MSButton;
import mastip.com.moneysaver.support.extend.view.MSTextView;

/**
 * Created by Tivo Yudha on 24/04/2017.
 */

public class PagerWelcomeAdapter extends PagerAdapter {

    private ArrayList<String> listWord = new ArrayList<>();
    private Context context;

    private MSTextView tvWelcome;
    private MSButton buttonWelcome;

    public PagerWelcomeAdapter(Context context, ArrayList listWord) {
        this.context = context;
        this.listWord = listWord;
    }

    @Override
    public int getCount() {
        return listWord.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_welcome_pager, null);
        tvWelcome = (MSTextView) v.findViewById(R.id.tvWelcome);
        buttonWelcome = (MSButton) v.findViewById(R.id.buttonWelcome);

        tvWelcome.setText(listWord.get(position));
        if(position != (listWord.size() - 1)) {
            buttonWelcome.setVisibility(View.INVISIBLE);
        }

        container.addView(v);

        return v;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (FrameLayout)object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout)object);
    }
}

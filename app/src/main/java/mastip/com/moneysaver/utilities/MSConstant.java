package mastip.com.moneysaver.utilities;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class MSConstant {

    public static final String SHARED_PREFERENCES_KEY = "moneysaver";

    public static final String SHARED_PREF_KEY_IS_FIRST_USE = "is_first_use";

    public static final String DATE_FORMAT = "dd-MMMM-yyyy hh:mm:ss aaa";
    public static final String DATE_FORMAT_WITHOUT_HOURS = "dd-MMMM-yyyy";

    public static final int RESULT_PASSWORD1 = 101;
    public static final int RESULT_PASSWORD2 = 102;
    public static final int RESULT_PASSWORD3 = 103;

    public static final String TAG_PASSWORD = "password";
}

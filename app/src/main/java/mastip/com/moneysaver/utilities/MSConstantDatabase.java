package mastip.com.moneysaver.utilities;

/**
 * Created by HateLogcatError on 11/8/2017.
 */

public class MSConstantDatabase {

    public static final String DATABASE_NAME = "moneysaver3.db";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_USER = "tb_user";
    public static final String TABLE_TRANSACTION = "tb_transaction";

    public static String USER_ID = "user_id";
    public static String USER_FULLNAME = "fullname";
    public static String USER_CREATED_AT = "user_created_at";
    public static String USER_CRESDENTIAL = "cresdential";
    public static String USER_BALANCE = "balance";

    public static String TRANSACTION_ID_TRANSACTION = "id_transaction";
    public static String TRANSACTION_TRANSACTION_TOTAL = "transaction_total";
    public static String TRANSACTION_NOTES = "notes";
    public static String TRANSACTION_CREATED_AT = "created_at";
    public static String TRANSACTION_TRANSACTION_MODE = "transaction_mode";
    public static String TRANSACTION_STATUS = "status";

    public static String CREATE_TB_TRANSACTION = "CREATE TABLE " + TABLE_TRANSACTION + " ("
            + TRANSACTION_ID_TRANSACTION + " integer primary key autoincrement,"
            + TRANSACTION_TRANSACTION_MODE + " integer,"
            + TRANSACTION_TRANSACTION_TOTAL + " integer,"
            + TRANSACTION_NOTES + " text,"
            + TRANSACTION_STATUS + " integer,"
            + TRANSACTION_CREATED_AT + " text)";

    public static String CREATE_TB_USER = "CREATE TABLE " + TABLE_USER + " ("
            + USER_ID + " integer primary key autoincrement,"
            + USER_FULLNAME + " text,"
            + USER_BALANCE + " integer,"
            + USER_CRESDENTIAL + " text,"
            + USER_CREATED_AT + " text)";

    public static String DROP_TB_USER = "DROP TABLE IF EXISTS " + TABLE_USER;
    public static String DROP_TB_TRANSACTION = "DROP TABLE IF EXISTS " + TABLE_TRANSACTION;
}

package mastip.com.moneysaver.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class MSSharedPreference {

    private static MSSharedPreference singleton;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public static MSSharedPreference getSingleton(Context context) {
        if(singleton == null) {
            singleton = new MSSharedPreference(context);
        }

        return singleton;
    }

    private MSSharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(MSConstant.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    public void setString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void setInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public void setBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void remove(String key) {
        editor.remove(key);
        editor.commit();
    }
}

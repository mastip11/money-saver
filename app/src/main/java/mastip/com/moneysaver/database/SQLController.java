package mastip.com.moneysaver.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.model.DataUserModel;
import mastip.com.moneysaver.model.TransactionModel;
import mastip.com.moneysaver.utilities.MSConstant;
import mastip.com.moneysaver.utilities.MSConstantDatabase;

/**
 * Created by Tivo Yudha on 26/04/2017.
 */

public class SQLController extends SQLiteOpenHelper {

    private Context context;

    private static SQLController singleton;

    public static SQLController getSingleton(Context context) {
        if(singleton == null) {
            singleton = new SQLController(context);
        }
        return singleton;
    }

    public SQLController(Context context) {
        super(context, MSConstantDatabase.DATABASE_NAME, null, MSConstantDatabase.DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(MSConstantDatabase.CREATE_TB_USER);
        sqLiteDatabase.execSQL(MSConstantDatabase.CREATE_TB_TRANSACTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(MSConstantDatabase.DROP_TB_USER);
        sqLiteDatabase.execSQL(MSConstantDatabase.DROP_TB_TRANSACTION);

        onCreate(sqLiteDatabase);
    }

    public void deleteUser() {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        sqLiteDatabase.delete(MSConstantDatabase.TABLE_USER, null, null);
        sqLiteDatabase.delete(MSConstantDatabase.TABLE_TRANSACTION, null, null);
        sqLiteDatabase.close();
    }

    private void initCresdential() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MSConstant.DATE_FORMAT_WITHOUT_HOURS, Locale.US);
        String time = simpleDateFormat.format(date);

        String select = "select * from " + MSConstantDatabase.TABLE_USER;

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery(select, null);

        if(cursor.getCount() == 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MSConstantDatabase.USER_FULLNAME, "Nameless");
            contentValues.put(MSConstantDatabase.USER_BALANCE, 0);
            contentValues.put(MSConstantDatabase.USER_CREATED_AT, time);
            contentValues.put(MSConstantDatabase.USER_CRESDENTIAL, "0000");

            sqLiteDatabase.insert(MSConstantDatabase.TABLE_USER, null, contentValues);
        }
    }

    public boolean isLoginAllowed(String number) {
        initCresdential();

        String select = "select * from " + MSConstantDatabase.TABLE_USER + " where " + MSConstantDatabase.USER_CRESDENTIAL + " = '" + number + "'";

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery(select, null);

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();

            DataUserModel.setUserID(cursor.getInt(0));
            DataUserModel.setFullname(cursor.getString(1));
            DataUserModel.setBalance(cursor.getInt(2));
            DataUserModel.setCreatedAt(cursor.getString(4));
        }

        return cursor.getCount() > 0;
    }

    public void addingTransaction(Integer userID, Integer mode, Integer transactionNominal, String notes) {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(MSConstant.DATE_FORMAT, Locale.US);
        String time = simpleDateFormat.format(date);

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MSConstantDatabase.TRANSACTION_TRANSACTION_MODE, mode);
        contentValues.put(MSConstantDatabase.TRANSACTION_TRANSACTION_TOTAL, transactionNominal);
        contentValues.put(MSConstantDatabase.TRANSACTION_NOTES, notes);
        contentValues.put(MSConstantDatabase.TRANSACTION_CREATED_AT, time);
        contentValues.put(MSConstantDatabase.TRANSACTION_STATUS, 1);

        sqLiteDatabase.insert(MSConstantDatabase.TABLE_TRANSACTION, null, contentValues);
        sqLiteDatabase.close();

        calculateNewBalance(userID, transactionNominal, mode);
    }

    private void calculateNewBalance(Integer userID, Integer nominal, Integer transactionMode) {
        String select = "select * from " + MSConstantDatabase.TABLE_USER + " where user_id = " + userID;
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(select, null);

        cursor.moveToFirst();
        Integer nowBalance = cursor.getInt(2);

        if(transactionMode == 1) {
            nowBalance += nominal;
        }
        else {
            nowBalance -= nominal;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(MSConstantDatabase.USER_BALANCE, nowBalance);

        sqLiteDatabase.update(MSConstantDatabase.TABLE_USER, contentValues, "user_id = " + userID, null);

        gettingNowDataUser(userID);
    }

    public ArrayList<DataUserModel> gettingNowDataUser(Integer userID) {
        ArrayList<DataUserModel> dataUserModels = new ArrayList<>();

        String select = "select * from " +MSConstantDatabase.TABLE_USER + " where user_id = " + userID + "";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(select, null);

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
        }

        return dataUserModels;
    }

    public ArrayList<TransactionModel> gettingDataHistory() {
        ArrayList<TransactionModel> transactionModels = new ArrayList<>();

        String select = "select * from " +MSConstantDatabase.TABLE_TRANSACTION + " where " + MSConstantDatabase.TRANSACTION_STATUS + " = 1";
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(select, null);

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            String mode;
            for(Integer a = 0; a < cursor.getCount(); a++) {
                if(cursor.getInt(1) == 1) {
                    mode = "Income";
                }
                else {
                    mode = "Expense";
                }

                TransactionModel transactionModel = new TransactionModel();
                transactionModel.setID(0);
                transactionModel.setBalance(cursor.getInt(2));
                transactionModel.setReason(cursor.getString(3));
                transactionModel.setType(mode);
                transactionModel.setTimeOccured(cursor.getString(5));

                transactionModels.add(transactionModel);
                cursor.moveToNext();
            }
        }
        return transactionModels;
    }

    public Boolean updateUser(String fullname, String password) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        if(password.equals("")) {
            password = "0000";
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(context.getString(R.string.fullname), fullname);
        contentValues.put(context.getString(R.string.password), password);

        Integer i = sqLiteDatabase.update(MSConstantDatabase.TABLE_USER, contentValues,  MSConstantDatabase.USER_ID + " = " + DataUserModel.getUserID() + "", null);

        return i > 0;
    }
}

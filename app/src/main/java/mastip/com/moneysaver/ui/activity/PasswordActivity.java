package mastip.com.moneysaver.ui.activity;

import android.content.Intent;

import butterknife.BindView;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.support.extend.view.MSLockScreen;
import mastip.com.moneysaver.ui.activity.controller.PasswordController;

/**
 * Created by HateLogcatError on 11/11/2017.
 */

public class PasswordActivity extends BaseAutoActivity {

    @BindView(R.id.login_lockscreen_mslockscreen)
    MSLockScreen lockScreen;

    private PasswordController controller;

    @Override
    protected void initBeforeViewCreated() {

    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_login;
    }

    @Override
    protected void initItem() {
        hideActionBar();

        controller = new PasswordController(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.onActivityResult(requestCode, resultCode, data);
    }

    public MSLockScreen getLockScreen() {
        return lockScreen;
    }
}

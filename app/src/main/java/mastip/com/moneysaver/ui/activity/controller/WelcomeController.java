package mastip.com.moneysaver.ui.activity.controller;

import android.content.Intent;

import java.util.ArrayList;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.adapter.PagerWelcomeAdapter;
import mastip.com.moneysaver.ui.activity.LoginActivity;
import mastip.com.moneysaver.ui.activity.WelcomeActivity;
import mastip.com.moneysaver.utilities.MSConstant;
import mastip.com.moneysaver.utilities.MSSharedPreference;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class WelcomeController {

    private WelcomeActivity activity;

    private ArrayList<String> listWord = new ArrayList<>();

    public WelcomeController(WelcomeActivity activity) {
        this.activity = activity;

        settingSharedPref();
        setListWord();
        setUpViewPager();
    }

    private void settingSharedPref() {
        MSSharedPreference.getSingleton(activity).setBoolean(MSConstant.SHARED_PREF_KEY_IS_FIRST_USE, false);
    }

    private void setUpViewPager() {
        PagerWelcomeAdapter adapter = new PagerWelcomeAdapter(activity, listWord);

        activity.getViewWelcome().setAdapter(adapter);
        activity.getTabWelcome().setupWithViewPager(activity.getViewWelcome());
    }

    private void setListWord() {
        listWord.add(activity.getResources().getString(R.string.welcome_message1));
        listWord.add(activity.getResources().getString(R.string.welcome_message2));
        listWord.add(activity.getResources().getString(R.string.welcome_message3));
        listWord.add(activity.getResources().getString(R.string.welcome_message4));
    }

    public void onAgreeClicked() {
        settingSharedPref();
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
}

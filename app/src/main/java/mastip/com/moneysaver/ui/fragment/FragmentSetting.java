package mastip.com.moneysaver.ui.fragment;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoFragment;
import mastip.com.moneysaver.ui.fragment.controller.FragmentSettingController;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSetting extends BaseAutoFragment {

    @BindView(R.id.setting_profile_relativelayout)
    RelativeLayout layoutProfile;
    @BindView(R.id.setting_about_relativelayout)
    RelativeLayout layoutAbout;

    private FragmentSettingController controller;

    @Override
    public int getLayout() {
        return R.layout.fragment_setting;
    }

    @Override
    public void initItem(View v) {
        controller = new FragmentSettingController(this);
    }

    public RelativeLayout getLayoutProfile() {
        return layoutProfile;
    }

    public RelativeLayout getLayoutAbout() {
        return layoutAbout;
    }
}

package mastip.com.moneysaver.ui.fragment.controller;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.model.DataHolder;
import mastip.com.moneysaver.ui.activity.LoginActivity;
import mastip.com.moneysaver.ui.activity.ProfileActivity;
import mastip.com.moneysaver.ui.fragment.FragmentSetting;
import mastip.com.moneysaver.utilities.MSConstant;
import mastip.com.moneysaver.utilities.MSSharedPreference;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class FragmentSettingController {

    private FragmentSetting fragment;

    public FragmentSettingController(FragmentSetting fragment) {
        this.fragment = fragment;
        init();
    }

    private void init() {
        fragment.getLayoutProfile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(fragment.getContext(), ProfileActivity.class);
                fragment.startActivity(intent);
                fragment.getActivity().finish();
            }
        });

        fragment.getLayoutAbout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(fragment.getContext());
                alertDialog.setTitle(fragment.getResources().getString(R.string.text_title_about));
                alertDialog.setMessage(fragment.getResources().getString(R.string.text_message_about));
                alertDialog.setPositiveButton(fragment.getResources().getString(R.string.text_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.setCancelable(false);
                alertDialog.show();
            }
        });
    }
}

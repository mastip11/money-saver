package mastip.com.moneysaver.ui.fragment.controller;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import mastip.com.moneysaver.adapter.ListHistoryAdapter;
import mastip.com.moneysaver.database.SQLController;
import mastip.com.moneysaver.model.TransactionModel;
import mastip.com.moneysaver.ui.fragment.FragmentHistoryList;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class FragmentHistoryController {

    private FragmentHistoryList fragment;

    private ArrayList<TransactionModel> allData = new ArrayList<>();

    public FragmentHistoryController(FragmentHistoryList fragment) {
        this.fragment = fragment;

        settingRecyclerView();
    }

    private void gettingHistoryData() {
        allData.clear();
        allData = SQLController.getSingleton(fragment.getContext()).gettingDataHistory();
    }

    private void settingRecyclerView() {
        gettingHistoryData();

        ListHistoryAdapter adapter = new ListHistoryAdapter(fragment.getContext(), allData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(fragment.getContext());

        fragment.getListHistory().setHasFixedSize(true);
        fragment.getListHistory().setLayoutManager(layoutManager);
        fragment.getListHistory().setItemAnimator(new DefaultItemAnimator());
        fragment.getListHistory().setAdapter(adapter);
    }
}

package mastip.com.moneysaver.ui.fragment.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;

import java.util.Date;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.model.DataUserModel;
import mastip.com.moneysaver.ui.activity.AddingTransactionActivity;
import mastip.com.moneysaver.ui.fragment.FragmentMainMenu;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class FragmentMainMenuController {

    private FragmentMainMenu fragment;

    public FragmentMainMenuController(FragmentMainMenu fragment) {
        this.fragment = fragment;

        init();
    }

    private void init() {
        fragment.getTvBalance().setText("Rp. " + DataUserModel.getBalance());
        fragment.getTvDateCreated().setText(DataUserModel.getCreatedAt());
        settingTime();
        settingMoneyEmoticon();

        fragment.getFab().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(fragment.getContext(), AddingTransactionActivity.class);
                fragment.startActivity(intent);
            }
        });
    }

    private Integer readingTime() {
        Date date = new Date(System.currentTimeMillis());
        return date.getHours();
    }

    private void settingTime() {
        Bitmap bitmap = null;
        if(readingTime() < 12 && readingTime() > 4) {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_sunshine)).getBitmap();
            fragment.getTvGreetingMsg().setText(fragment.getString(R.string.text_morning) + DataUserModel.getFullname());
        }
        else if(readingTime() > 12 && readingTime() < 18) {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_sunshine)).getBitmap();
            fragment.getTvGreetingMsg().setText(fragment.getString(R.string.text_afternoon) + DataUserModel.getFullname());
        }
        else {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_night)).getBitmap();
            fragment.getTvGreetingMsg().setText(fragment.getString(R.string.text_night) + DataUserModel.getFullname());
        }

        fragment.getIvTimeIcon().setImageBitmap(bitmap);
    }

    private void settingMoneyEmoticon() {
        Bitmap bitmap = null;
        String text = "";
        if (DataUserModel.getBalance() == 0 && DataUserModel.getBalance() < 50001) {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_normal_face)).getBitmap();
            text = fragment.getString(R.string.text_money_warning3);
        } else if (DataUserModel.getBalance() < 0) {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_unhappy)).getBitmap();
            text = fragment.getString(R.string.text_money_warning4);
        } else if (DataUserModel.getBalance() > 50001) {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_happy)).getBitmap();
            text = fragment.getString(R.string.text_money_warning2);
        } else if (DataUserModel.getBalance() > 150001) {
            bitmap = ((BitmapDrawable) fragment.getContext().getDrawable(R.drawable.ic_very_happy)).getBitmap();
            text = fragment.getString(R.string.text_money_warning1);
        }

        fragment.getIvEmoticon().setImageBitmap(bitmap);
        fragment.getTvEmoticonReminder().setText(text);
    }
}

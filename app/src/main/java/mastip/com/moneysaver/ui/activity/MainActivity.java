package mastip.com.moneysaver.ui.activity;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.ui.activity.controller.MainController;

public class MainActivity extends BaseAutoActivity {

    private MainController controller;

    @Override
    public void initBeforeViewCreated() {
        setFullScreen();
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    public void initItem() {
        hideActionBar();

        this.controller = new MainController(this);
    }
}

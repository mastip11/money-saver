package mastip.com.moneysaver.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.ui.activity.controller.WelcomeController;

public class WelcomeActivity extends BaseAutoActivity {

    ViewPager viewWelcome;
    TabLayout tabWelcome;

    private WelcomeController controller;

    @Override
    public void initBeforeViewCreated() {

    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_welcome;
    }

    @Override
    public void initItem() {
        viewWelcome = (ViewPager) findViewById(R.id.viewPagerWelcome);
        tabWelcome = (TabLayout) findViewById(R.id.tabLayoutWelcome);

        hideActionBar();

        controller = new WelcomeController(this);
    }

    public void onAgreeToUseClicked(View view) {
        controller.onAgreeClicked();
    }

    public ViewPager getViewWelcome() {
        return viewWelcome;
    }

    public TabLayout getTabWelcome() {
        return tabWelcome;
    }
}

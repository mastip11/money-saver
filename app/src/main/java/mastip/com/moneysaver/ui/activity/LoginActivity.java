package mastip.com.moneysaver.ui.activity;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.support.extend.view.MSLockScreen;
import mastip.com.moneysaver.ui.activity.controller.LoginController;

public class LoginActivity extends BaseAutoActivity {

    private MSLockScreen lockScreen;
    private LoginController controller;

    @Override
    protected void initBeforeViewCreated() {

    }

    @Override
    protected int getLayoutID() {
        return R.layout.activity_login;
    }

    @Override
    protected void initItem() {
        lockScreen = (MSLockScreen) findViewById(R.id.login_lockscreen_mslockscreen);

        controller = new LoginController(this);
    }

    public MSLockScreen getLockScreen() {
        return lockScreen;
    }
}

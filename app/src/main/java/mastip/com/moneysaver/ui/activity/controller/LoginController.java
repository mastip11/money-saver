package mastip.com.moneysaver.ui.activity.controller;

import android.content.Intent;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.database.SQLController;
import mastip.com.moneysaver.support.extend.view.listener.LockScreenListener;
import mastip.com.moneysaver.ui.activity.LoginActivity;
import mastip.com.moneysaver.ui.activity.MainMenuActivity;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class LoginController {

    private LoginActivity activity;

    public LoginController(LoginActivity activity) {
        this.activity = activity;

        activity.hideActionBar();
        initOnLockListener();
    }

    private void initOnLockListener() {
        activity.getLockScreen().setOnLockScreenResult(new LockScreenListener() {
            @Override
            public void onLockScreenResult(String number) {
                doingLogin(number);
            }
        });
    }

    private void doingLogin(String passcode) {
        if(SQLController.getSingleton(activity).isLoginAllowed(passcode)) {
            Intent intent = new Intent(activity, MainMenuActivity.class);
            activity.startActivity(intent);
            activity.finish();
        }
        else {
            activity.showToast(activity.getString(R.string.text_login_failed));
        }
    }
}

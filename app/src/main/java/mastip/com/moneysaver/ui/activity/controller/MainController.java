package mastip.com.moneysaver.ui.activity.controller;

import android.content.Intent;

import mastip.com.moneysaver.database.SQLController;
import mastip.com.moneysaver.ui.activity.LoginActivity;
import mastip.com.moneysaver.ui.activity.MainActivity;
import mastip.com.moneysaver.ui.activity.WelcomeActivity;
import mastip.com.moneysaver.utilities.MSConstant;
import mastip.com.moneysaver.utilities.MSSharedPreference;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class MainController {

    private MainActivity activity;

    public MainController(MainActivity activity) {
        this.activity = activity;

        settingDatabase();
        setCounterToNextActivity();
    }

    private void settingDatabase() {
        new SQLController(activity);
    }

    private Boolean checkIfFirstUseOrNot() {
        return MSSharedPreference.getSingleton(activity).getBoolean(MSConstant.SHARED_PREF_KEY_IS_FIRST_USE);
    }

    private void setCounterToNextActivity() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    Intent intent = null;
                    if(checkIfFirstUseOrNot()) {
                        intent = new Intent(activity, WelcomeActivity.class);
                    }
                    else {
                        intent = new Intent(activity, LoginActivity.class);
                    }

                    activity.startActivity(intent);
                    activity.finish();
                }
            }
        });
        thread.start();
    }
}

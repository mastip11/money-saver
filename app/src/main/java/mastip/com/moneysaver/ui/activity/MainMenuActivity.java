package mastip.com.moneysaver.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import butterknife.BindView;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.ui.activity.controller.MainMenuController;

public class MainMenuActivity extends BaseAutoActivity {

    @BindView(R.id.viewPagerMainMenu)
    ViewPager viewPager;
    @BindView(R.id.tabLayoutMainMenu)
    TabLayout tabLayout;

    private MainMenuController controller;

    @Override
    public void initBeforeViewCreated() {

    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_main_menu;
    }

    @Override
    public void initItem() {
        hideActionBar();

        controller = new MainMenuController(this);
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }
}

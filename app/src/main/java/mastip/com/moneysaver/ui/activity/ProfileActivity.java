package mastip.com.moneysaver.ui.activity;

import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.support.extend.view.MSButton;
import mastip.com.moneysaver.ui.activity.controller.ProfileController;

public class ProfileActivity extends BaseAutoActivity {

    @BindView(R.id.etFullnameUpdate)
    EditText etFullname;
    @BindView(R.id.profile_update_msbutton)
    MSButton updateButton;
    @BindView(R.id.profile_changepassword_msbutton)
    MSButton changePasswordButton;

    private ProfileController controller;

    @Override
    public void initBeforeViewCreated() {

    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_profile;
    }

    @Override
    public void initItem() {
        hideActionBar();

        controller = new ProfileController(this);
    }

    public EditText getEtFullname() {
        return etFullname;
    }

    public MSButton getUpdateButton() {
        return updateButton;
    }

    public MSButton getChangePasswordButton() {
        return changePasswordButton;
    }
}

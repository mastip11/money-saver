package mastip.com.moneysaver.ui.fragment;


import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoFragment;
import mastip.com.moneysaver.ui.fragment.controller.FragmentHistoryController;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHistoryList extends BaseAutoFragment {

    RecyclerView listHistory;

    private FragmentHistoryController controller;

    @Override
    public int getLayout() {
        return R.layout.fragment_history_list;
    }

    @Override
    public void initItem(View v) {
        listHistory = (RecyclerView) v.findViewById(R.id.listHistoryTransaction);

        controller = new FragmentHistoryController(this);
    }

    public RecyclerView getListHistory() {
        return listHistory;
    }
}

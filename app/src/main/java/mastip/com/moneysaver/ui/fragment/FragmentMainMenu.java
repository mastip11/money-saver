package mastip.com.moneysaver.ui.fragment;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoFragment;
import mastip.com.moneysaver.support.extend.view.MSTextView;
import mastip.com.moneysaver.ui.fragment.controller.FragmentMainMenuController;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMainMenu extends BaseAutoFragment {

    private MSTextView tvBalance, tvAccountLifetime, tvGreetingMsg, tvEmoticonReminder, tvDateCreated;
    private ImageView ivTimeIcon, ivEmoticon;
    private FloatingActionButton fab;

    private FragmentMainMenuController controller;

    @Override
    public int getLayout() {
        return R.layout.fragment_main_menu;
    }

    @Override
    public void initItem(View v) {
        tvGreetingMsg = (MSTextView) v.findViewById(R.id.mainmenu_greting_mstextview);
        tvBalance = (MSTextView) v.findViewById(R.id.textMoneyBalance);
        tvAccountLifetime = (MSTextView) v.findViewById(R.id.tvLongAccountCreatedMainMenu);
        ivTimeIcon = (ImageView) v.findViewById(R.id.ivTimePicture);
        tvEmoticonReminder = (MSTextView) v.findViewById(R.id.tvCurrentMoney);
        ivEmoticon = (ImageView) v.findViewById(R.id.ivCurrentMoneyLogo);
        fab = (FloatingActionButton) v.findViewById(R.id.mainmenu_fab_floatingactionbutton);
        tvDateCreated = (MSTextView) v.findViewById(R.id.mainmenu_textdatecreated_mstextview);

        controller = new FragmentMainMenuController(this);
    }

    public TextView getTvBalance() {
        return tvBalance;
    }

    public TextView getTvGreetingMsg() {
        return tvGreetingMsg;
    }

    public TextView getTvEmoticonReminder() {
        return tvEmoticonReminder;
    }

    public ImageView getIvTimeIcon() {
        return ivTimeIcon;
    }

    public ImageView getIvEmoticon() {
        return ivEmoticon;
    }

    public FloatingActionButton getFab() {
        return fab;
    }

    public MSTextView getTvDateCreated() {
        return tvDateCreated;
    }
}

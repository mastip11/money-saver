package mastip.com.moneysaver.ui.activity.controller;

import android.content.Intent;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.database.SQLController;
import mastip.com.moneysaver.model.DataUserModel;
import mastip.com.moneysaver.ui.activity.AddingTransactionActivity;
import mastip.com.moneysaver.ui.activity.MainMenuActivity;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class AddingTransactionController {

    private AddingTransactionActivity activity;

    Integer transactionValue;
    Integer nominal;
    String memo;

    public AddingTransactionController(AddingTransactionActivity activity) {
        this.activity = activity;
        ButterKnife.bind(this, activity);

        onAddTransaction();
    }

    private void gettingAllValues() {
        if(activity.getRbIncome().isChecked()) {
            transactionValue = 1;
        }
        else {
            transactionValue = 2;
        }
        nominal = Integer.valueOf(activity.getEtTransactionNominal().getText().toString());
        if(activity.getEtMemo().getText().length() < 1) {
            memo = "-";
        }
        else {
            memo = activity.getEtMemo().getText().toString();
        }
    }

    @OnClick(R.id.addtransaction_submit_msbutton)
    void onAddTransaction() {
        activity.getSubmit().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gettingAllValues();
                SQLController.getSingleton(activity).addingTransaction(DataUserModel.getUserID(), transactionValue, nominal, memo);

                Intent intent = new Intent(activity, MainMenuActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        });
    }
}

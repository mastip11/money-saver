package mastip.com.moneysaver.ui.activity.controller;

import android.content.Intent;

import butterknife.OnClick;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.database.SQLController;
import mastip.com.moneysaver.model.DataUserModel;
import mastip.com.moneysaver.ui.activity.MainMenuActivity;
import mastip.com.moneysaver.ui.activity.ProfileActivity;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class ProfileController {

    private ProfileActivity activity;

    String fullname, password, newPassword;

    public ProfileController(ProfileActivity activity) {
        this.activity = activity;

        settingTextToContent();
    }

    private void settingTextToContent() {
        activity.getEtFullname().setText(DataUserModel.getFullname());
    }

    private String validatingData() {
        fullname = activity.getEtFullname().getText().toString();

        if(fullname.length() < 5) {
            return activity.getString(R.string.text_fullname) + activity.getString(R.string.text_warning_less_than_5);
        }
        else {
            return activity.getString(R.string.text_OK);
        }
    }

    private void checkIfUpdateIsSuccess() {
        if(new SQLController(activity).updateUser(fullname, newPassword)) {
            activity.showToast(activity.getString(R.string.text_update_success));
            activity.finish();
        }
        else {
            activity.showToast(activity.getString(R.string.text_update_failed));
        }
    }

    @OnClick(R.id.profile_update_msbutton)
    void onUpdate() {
        String message = validatingData();
        if(message.equals(activity.getString(R.string.text_OK))) {
            checkIfUpdateIsSuccess();
        }
        else {
            activity.showToast(message);
        }
    }

    @OnClick(R.id.profile_changepassword_msbutton)
    void onChangePassword() {

    }
}

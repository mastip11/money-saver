package mastip.com.moneysaver.ui.activity.controller;

import android.app.Activity;
import android.content.Intent;

import mastip.com.moneysaver.support.extend.view.listener.LockScreenListener;
import mastip.com.moneysaver.ui.activity.PasswordActivity;
import mastip.com.moneysaver.utilities.MSConstant;

/**
 * Created by HateLogcatError on 11/11/2017.
 */

public class PasswordController {

    private PasswordActivity activity;

    private String password = "";

    public PasswordController(PasswordActivity activity) {
        this.activity = activity;

        initLockScreen();
    }

    private void initLockScreen() {
        activity.getLockScreen().setOnLockScreenResult(new LockScreenListener() {
            @Override
            public void onLockScreenResult(String number) {

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == MSConstant.RESULT_PASSWORD1 && resultCode == Activity.RESULT_OK && data != null) {
            password = data.getStringExtra(MSConstant.TAG_PASSWORD);


        }
        else if(requestCode == MSConstant.RESULT_PASSWORD2 && resultCode == Activity.RESULT_OK && data != null) {
            password = data.getStringExtra(MSConstant.TAG_PASSWORD);
        }
        else if(requestCode == MSConstant.RESULT_PASSWORD3 && resultCode == Activity.RESULT_OK && data != null) {
            password = data.getStringExtra(MSConstant.TAG_PASSWORD);
        }
    }
}

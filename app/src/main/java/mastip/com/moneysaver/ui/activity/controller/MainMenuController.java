package mastip.com.moneysaver.ui.activity.controller;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.adapter.FragmentAdapter;
import mastip.com.moneysaver.ui.activity.MainMenuActivity;
import mastip.com.moneysaver.ui.fragment.FragmentHistoryList;
import mastip.com.moneysaver.ui.fragment.FragmentMainMenu;
import mastip.com.moneysaver.ui.fragment.FragmentSetting;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class MainMenuController {

    private MainMenuActivity activity;

    private ArrayList<Fragment> fragments = new ArrayList<>();

    public MainMenuController(MainMenuActivity activity) {
        this.activity = activity;

        initViewPager();
    }

    private void initViewPager() {
        initFragments();

        FragmentAdapter adapter = new FragmentAdapter(activity.getSupportFragmentManager(), fragments);

        activity.getViewPager().setAdapter(adapter);
        activity.getTabLayout().setupWithViewPager(activity.getViewPager());
        activity.getTabLayout().removeAllTabs();

        activity.getTabLayout().addTab(activity.getTabLayout().newTab().setIcon(activity.getResources().getDrawable(R.drawable.ic_info)));
        activity.getTabLayout().addTab(activity.getTabLayout().newTab().setIcon(activity.getResources().getDrawable(R.drawable.ic_list)));
        activity.getTabLayout().addTab(activity.getTabLayout().newTab().setIcon(activity.getResources().getDrawable(R.drawable.ic_setting)));
    }

    private void initFragments() {
        fragments.add(new FragmentMainMenu());
        fragments.add(new FragmentHistoryList());
        fragments.add(new FragmentSetting());
    }
}

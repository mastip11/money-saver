package mastip.com.moneysaver.ui.activity;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.base.BaseAutoActivity;
import mastip.com.moneysaver.support.extend.view.MSButton;
import mastip.com.moneysaver.support.extend.view.MSEditText;
import mastip.com.moneysaver.ui.activity.controller.AddingTransactionController;

public class AddingTransactionActivity extends BaseAutoActivity {

    private RadioButton rbIncome, rbExpense;
    private MSEditText etTransactionNominal, etMemo;

    private MSButton submit;

    private AddingTransactionController controller;

    @Override
    public void initBeforeViewCreated() {

    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_adding_transaction;
    }

    @Override
    public void initItem() {
        rbIncome = (RadioButton) findViewById(R.id.rbIncome);
        rbExpense = (RadioButton) findViewById(R.id.rbExpenses);
        etTransactionNominal = (MSEditText) findViewById(R.id.etBalanceAddTransaction);
        etMemo = (MSEditText) findViewById(R.id.etNotesAddTrans);
        submit = (MSButton) findViewById(R.id.addtransaction_submit_msbutton);

        controller = new AddingTransactionController(this);
    }

    public EditText getEtMemo() {
        return etMemo;
    }

    public EditText getEtTransactionNominal() {
        return etTransactionNominal;
    }

    public RadioButton getRbExpense() {
        return rbExpense;
    }

    public RadioButton getRbIncome() {
        return rbIncome;
    }

    public MSButton getSubmit() {
        return submit;
    }
}

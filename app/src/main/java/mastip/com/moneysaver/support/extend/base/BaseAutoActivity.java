package mastip.com.moneysaver.support.extend.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import mastip.com.moneysaver.R;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public abstract class BaseAutoActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBeforeViewCreated();
        setContentView(getLayoutID());

        initProgressDialog();

        initItem();
    }

    public void setFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
    }

    public void showLoading(String message) {
        progressDialog.setTitle(getString(R.string.text_please_wait));
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void dismissLoading() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message + "", Toast.LENGTH_SHORT).show();
    }

    protected abstract void initBeforeViewCreated();
    protected abstract int getLayoutID();
    protected abstract void initItem();
}

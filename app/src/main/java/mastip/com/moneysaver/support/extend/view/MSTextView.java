package mastip.com.moneysaver.support.extend.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class MSTextView extends TextView {

    private Context context;

    public MSTextView(Context context) {
        super(context);
        this.context = context;

        initItem();
    }

    public MSTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        initItem();
    }

    public MSTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;

        initItem();
    }

    private void initItem() {
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Ubuntu-Light.ttf"));
    }
}

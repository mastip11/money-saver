package mastip.com.moneysaver.support.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import mastip.com.moneysaver.R;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class MSAlertDialog {

    private Context context;

    public MSAlertDialog(Context context) {
        this.context = context;
    }

    public void showDialog(String title, String message, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener) {
        AlertDialog.Builder alertDeleteDate = new AlertDialog.Builder(context);
        alertDeleteDate.setTitle(title);
        alertDeleteDate.setMessage(message);
        alertDeleteDate.setCancelable(false);
        alertDeleteDate.setPositiveButton(context.getString(R.string.text_yes), yesListener);
        alertDeleteDate.setNegativeButton(context.getString(R.string.text_no), noListener);
        alertDeleteDate.show();
    }

    public void showDialog(String title, String message, DialogInterface.OnClickListener yesListener) {
        AlertDialog.Builder alertDeleteDate = new AlertDialog.Builder(context);
        alertDeleteDate.setTitle(title);
        alertDeleteDate.setMessage(message);
        alertDeleteDate.setCancelable(false);
        alertDeleteDate.setPositiveButton(context.getString(R.string.text_yes), yesListener);
        alertDeleteDate.setNegativeButton(context.getString(R.string.text_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDeleteDate.show();
    }
}

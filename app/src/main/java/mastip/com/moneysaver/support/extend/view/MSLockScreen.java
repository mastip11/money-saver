package mastip.com.moneysaver.support.extend.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mastip.com.moneysaver.R;
import mastip.com.moneysaver.support.extend.view.listener.LockScreenListener;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public class MSLockScreen extends RelativeLayout {

    @BindView(R.id.lockscreen_digit1_imageview)
    ImageView digitOne;
    @BindView(R.id.lockscreen_digit2_imageview)
    ImageView digitTwo;
    @BindView(R.id.lockscreen_digit3_imageview)
    ImageView digitThree;
    @BindView(R.id.lockscreen_digit4_imageview)
    ImageView digitFour;

    private LockScreenListener listener;

    private StringBuilder stringBuilder = new StringBuilder();

    private Context context;

    public MSLockScreen(Context context) {
        super(context);
        this.context = context;
        initView(context);
    }

    public MSLockScreen(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView(context);
    }

    public MSLockScreen(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView(context);
    }

    private void initView(Context context) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_lockscreen, this);
        ButterKnife.bind(this, v);
    }

    @OnClick(R.id.lockscreen_one_msbutton)
    void onOneButton() {
        onAddNumber("1");
    }

    @OnClick(R.id.lockscreen_two_msbutton)
    void onTwoButton() {
        onAddNumber("2");
    }

    @OnClick(R.id.lockscreen_three_msbutton)
    void onThreeButton() {
        onAddNumber("3");
    }

    @OnClick(R.id.lockscreen_four_msbutton)
    void onFourButton() {
        onAddNumber("4");
    }

    @OnClick(R.id.lockscreen_five_msbutton)
    void onFiveButton() {
        onAddNumber("5");
    }

    @OnClick(R.id.lockscreen_six_msbutton)
    void onSixButton() {
        onAddNumber("6");
    }

    @OnClick(R.id.lockscreen_seven_msbutton)
    void onSevenButton() {
        onAddNumber("7");
    }

    @OnClick(R.id.lockscreen_eight_msbutton)
    void onEightButton() {
        onAddNumber("8");
    }

    @OnClick(R.id.lockscreen_nine_msbutton)
    void onNineButton() {
        onAddNumber("9");
    }

    @OnClick(R.id.lockscreen_zero_msbutton)
    void onZeroButton() {
        onAddNumber("0");
    }

    @OnClick(R.id.lockscreen_clearall_msbutton)
    void onClearAllButton() {
        clearAllNumber();
    }

    @OnClick(R.id.lockscreen_erase_msbutton)
    void onEraseButton() {
        eraseOneCharacter();
    }

    private void onAddNumber(String number) {
        stringBuilder.append(number);

        initDigitView();
        validation();
    }

    private void clearAllNumber() {
        stringBuilder.delete(0, 4);

        initDigitView();
    }

    private void eraseOneCharacter() {
        if(stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        initDigitView();
    }

    private void validation() {
        if(stringBuilder.length() == 4) {
            listener.onLockScreenResult(stringBuilder.toString());

            clearAllNumber();
        }
    }

    private void initDigitView() {
        if(stringBuilder.length() == 0) {
            digitOne.setBackground(context.getDrawable(R.drawable.digit_unactive));
            digitTwo.setBackground(context.getDrawable(R.drawable.digit_unactive));
            digitThree.setBackground(context.getDrawable(R.drawable.digit_unactive));
            digitFour.setBackground(context.getDrawable(R.drawable.digit_unactive));

        }
        else if(stringBuilder.length() == 1) {
            digitOne.setBackground(context.getDrawable(R.drawable.digit_active));
            digitTwo.setBackground(context.getDrawable(R.drawable.digit_unactive));
            digitThree.setBackground(context.getDrawable(R.drawable.digit_unactive));
            digitFour.setBackground(context.getDrawable(R.drawable.digit_unactive));
        }
        else if(stringBuilder.length() == 2) {
            digitOne.setBackground(context.getDrawable(R.drawable.digit_active));
            digitTwo.setBackground(context.getDrawable(R.drawable.digit_active));
            digitThree.setBackground(context.getDrawable(R.drawable.digit_unactive));
            digitFour.setBackground(context.getDrawable(R.drawable.digit_unactive));
        }
        else if(stringBuilder.length() == 3) {
            digitOne.setBackground(context.getDrawable(R.drawable.digit_active));
            digitTwo.setBackground(context.getDrawable(R.drawable.digit_active));
            digitThree.setBackground(context.getDrawable(R.drawable.digit_active));
            digitFour.setBackground(context.getDrawable(R.drawable.digit_unactive));
        }
        else if(stringBuilder.length() == 4) {
            digitOne.setBackground(context.getDrawable(R.drawable.digit_active));
            digitTwo.setBackground(context.getDrawable(R.drawable.digit_active));
            digitThree.setBackground(context.getDrawable(R.drawable.digit_active));
            digitFour.setBackground(context.getDrawable(R.drawable.digit_active));
        }
    }

    public void setOnLockScreenResult(LockScreenListener listener) {
        this.listener = listener;
    }
}

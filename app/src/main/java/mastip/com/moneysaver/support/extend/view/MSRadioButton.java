package mastip.com.moneysaver.support.extend.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class MSRadioButton extends android.support.v7.widget.AppCompatRadioButton {

    public MSRadioButton(Context context) {
        super(context);
        initItem(context);
    }

    public MSRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initItem(context);
    }

    public MSRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initItem(context);
    }

    private void initItem(Context context) {
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Ubuntu-Light.ttf"));
    }
}

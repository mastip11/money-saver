package mastip.com.moneysaver.support.extend.view.listener;

/**
 * Created by HateLogcatError on 11/6/2017.
 */

public interface LockScreenListener {
    void onLockScreenResult(String number);
}

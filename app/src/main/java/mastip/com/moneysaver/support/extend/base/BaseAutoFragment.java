package mastip.com.moneysaver.support.extend.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import mastip.com.moneysaver.R;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public abstract class BaseAutoFragment extends Fragment {

    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(getLayout(), container, false);

        initItem(v);

        return v;
    }

    public void showLoading(String message) {
        progressDialog.setTitle(getString(R.string.text_please_wait));
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void dismissLoading() {
        if(progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showToast(String message) {
        Toast.makeText(getContext(), message + "", Toast.LENGTH_SHORT).show();
    }

    public abstract int getLayout();
    public abstract void initItem(View v);
}

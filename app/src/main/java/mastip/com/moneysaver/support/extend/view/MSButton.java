package mastip.com.moneysaver.support.extend.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by HateLogcatError on 11/5/2017.
 */

public class MSButton extends android.support.v7.widget.AppCompatButton {

    public MSButton(Context context) {
        super(context);
        initItem(context);
    }

    public MSButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initItem(context);
    }

    public MSButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initItem(context);
    }

    private void initItem(Context context) {
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Ubuntu-Light.ttf"));
    }
}
